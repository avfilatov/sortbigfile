DROP TABLE IF EXISTS files;
DROP TABLE IF EXISTS app_user_role;
DROP TABLE IF EXISTS app_user;

CREATE TABLE app_user(
    id SERIAL PRIMARY KEY,
    login varchar(50) NOT NULL UNIQUE,
    password varchar(100) NOT NULL,
    name varchar(50) NOT NULL,
    enabled boolean NOT NULL DEFAULT true
);
COMMENT ON TABLE app_user IS 'Application users';
COMMENT ON COLUMN app_user.password IS 'BCrypt encrypted password';

CREATE TABLE app_user_role (
--     login varchar(50) NOT NULL REFERENCES app_user(login),
    id integer REFERENCES app_user(id),
    role varchar(50) NOT NULL
);
COMMENT ON TABLE app_user_role IS 'Authority roles for Spring Security';

CREATE TABLE files (
    id SERIAL PRIMARY KEY,
    sender integer NOT NULL,
    source_path VARCHAR(250) NOT NULL,
    size BIGINT NOT NULL,
    last_modified BIGINT NOT NULL,
    status VARCHAR(50) NOT NULL,
    enter_queue BIGINT DEFAULT NULL,
    start_processing BIGINT DEFAULT NULL,
    finish_processing BIGINT DEFAULT NULL,
    target_path VARCHAR(250) DEFAULT NULL,
    target_url VARCHAR(250) DEFAULT NULL
);
COMMENT ON TABLE files IS 'Sorted files';
