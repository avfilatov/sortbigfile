INSERT INTO app_user(login, password, name, enabled) VALUES ('admin', '$2y$12$Zy40WW5ijF2Phhbksi2n9eGKzVOymxStpVgy3qSGP679PSzYbZsRC', 'Administrator', true);
INSERT INTO app_user_role(id, role) SELECT id, 'ROLE_USER' FROM app_user WHERE login='admin';
INSERT INTO app_user_role(id, role) SELECT id, 'ROLE_ADMIN' FROM app_user WHERE login='admin';

INSERT INTO app_user(login, password, name, enabled) VALUES ('user', '$2y$12$3bq/0JgdYj/t4xcWmmDGUuAN6JUadbLvQXbMFGMMME04el2ROYib.', 'Dear User', true);
INSERT INTO app_user_role(id, role) SELECT id, 'ROLE_USER' FROM app_user WHERE login='user';
