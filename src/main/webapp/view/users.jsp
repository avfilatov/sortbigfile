<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.luxoft.javaupskilling.sortbigfile.domain.AuthorityRole" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
    <h1>Users</h1>

    <div>
        <h2>Add new user:</h2>
        <form action="users/add" method="post">
            <table>
                <tr>
                    <td>Login:</td>
                    <td><input type="text" name="login" value="" required="true"></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td><input type="password" name="password" required="true"/></td>
                </tr>
                <tr>
                    <td>Confirm password:</td>
                    <td><input type="password" name="confirm_password" required="true"/></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" name="name" value="" required="true"/></td>
                </tr>
                <tr>
                    <td>Roles:</td>
<%--                    <td><input type='text' name='roles' required="true"/></td>--%>
                    <td>
                        <select size="3" multiple name="roles">
                            <option disabled>Choose roles</option>
                            <option selected value="${AuthorityRole.USER}"><c:out value="${AuthorityRole.USER}"/></option>
                            <option value="${AuthorityRole.ADMIN}"><c:out value="${AuthorityRole.ADMIN}"/></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td/>
                    <td><button type="submit">Add</button></td>
                </tr>
            </table>

            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>
        <div>
            <%
                if (request.getAttribute("error") != null) {
                    out.println(request.getAttribute("error"));
                }
                if (request.getAttribute("message") != null) {
                    out.println(request.getAttribute("message"));
                }
            %>
        </div>
    </div>

    <div>
        <table border="1" cellpadding="5">
            <caption><h2>List of users</h2></caption>
            <tr>
                <th>Id</th>
                <th>Login</th>
                <th>Name</th>
                <th>Enabled</th>
                <th>Roles</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="user" items="${appUsers}">
                <tr>
                    <td><c:out value="${user.id}" /></td>
                    <td><c:out value="${user.login}" /></td>
                    <td><c:out value="${user.name}" /></td>
                    <td><c:out value="${user.enabled}" /></td>
                    <td><c:out value="${user.roles}" /></td>
                    <td>
                        <form action="users/remove" method="post">
                            <input type="hidden" name="login" value="${user.login}"/>
                            <button type="submit">Delete</button>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        </form>
                        <c:if test="${user.enabled == false}">
                            <form action="users/enable" method="post">
                                <input type="hidden" name="login" value="${user.login}"/>
                                <button type="submit">Enable</button>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            </form>
                        </c:if>
                        <c:if test="${user.enabled == true}">
                            <form action="users/disable" method="post">
                                <input type="hidden" name="login" value="${user.login}"/>
                                <button type="submit">Disable</button>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            </form>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>
