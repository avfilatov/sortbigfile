<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Sort file</title>
</head>
<body>
    <h1>Sorting files</h1>
    <form action="files/sort" method="post">
        <p>Set path to your file:</p>
        <input type="text" name="pathToFile"/>
        <select size="1" name="actionType">
            <option disabled>Choose what to do</option>
            <option selected value="sort">Sort</option>
            <option value="generate">Generate</option>
        </select>
        <button type="submit">Send</button>
        <input type="hidden"
               name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
    <div>
        <%
            if (request.getAttribute("status") == "success") {
                out.println("Successful. Use reload to receive updates.");
            }
            if (request.getAttribute("error") != null) {
                out.println(request.getAttribute("error"));
            }
        %>
    </div>

    <div>
        <table border="1" cellpadding="5">
            <caption><h2>List of files</h2></caption>
            <tr>
                <th>User</th>
                <th>Source Path</th>
                <th>Size</th>
                <th>Last Modified</th>
                <th>Status</th>
                <th>Enter Queue</th>
                <th>Start Processing</th>
                <th>Finish Processing</th>
                <th>Target Path</th>
                <th>Target Url</th>
            </tr>
            <c:forEach var="fileInfo" items="${fileInfoList}">
                <tr>
                    <td><c:out value="${fileInfo.sender}" /></td>
                    <td><c:out value="${fileInfo.sourcePath}" /></td>
                    <td><c:out value="${fileInfo.size}" /></td>
                    <td><c:out value="${fileInfo.lastModified}" /></td>
                    <td><c:out value="${fileInfo.status}" /></td>
                    <td><c:out value="${fileInfo.enterQueue}" /></td>
                    <td><c:out value="${fileInfo.startProcessing}" /></td>
                    <td><c:out value="${fileInfo.finishProcessing}" /></td>
                    <td><c:out value="${fileInfo.targetPath}" /></td>
                    <td>
                        <c:if test="${fileInfo.targetUrl != null}">
                            <a href="<c:url value="${fileInfo.targetUrl}" />">Download</a>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

</body>
</html>
