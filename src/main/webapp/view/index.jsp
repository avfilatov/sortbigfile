<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <body>
        <h2>Hello, ${userName}</h2>
        <h2>Your authority role: ${userRoles}</h2>

        <div>
            <sec:authorize access="hasRole(T(com.luxoft.javaupskilling.sortbigfile.domain.AuthorityRole).ADMIN)">
                <a href="<c:url value="/users" />">Users</a>
                <br>
                <a href="<c:url value="/lbClassicStatus" />">Logback Status Messages</a>
            </sec:authorize>
        </div>
        <div>
            <sec:authorize access="hasRole(T(com.luxoft.javaupskilling.sortbigfile.domain.AuthorityRole).USER)">
                <a href="<c:url value="/files" />">Sort files</a>
                <br>
                <a href="<c:url value="/logout" />">Logout</a>
            </sec:authorize>
        </div>

<!--        <div>
            <sec:authorize access="!isAuthenticated()">
                <c:url value="/login" var="loginUrl" />
                <form action="${loginUrl}" method="get">
                    <input type="submit" value="Login" name="login">
                </form>
            </sec:authorize>

            <sec:authorize access="isAuthenticated()">
                <p>Ваш логин: <sec:authentication property="principal.username" /></p>
                <c:url value="/logout" var="logoutUrl" />
                <form action="${logoutUrl}" method="post">
                    <button type="submit">Logout</button>
                </form>
            </sec:authorize>
        </div>
-->

    </body>
</html>
