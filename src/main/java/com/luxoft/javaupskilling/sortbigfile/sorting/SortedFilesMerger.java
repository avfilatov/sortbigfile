package com.luxoft.javaupskilling.sortbigfile.sorting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

class SortedFilesMerger {
    private static final Logger logger = LoggerFactory.getLogger(SortedFilesMerger.class);

    // we need at least 3 files: for result, for segment and for file from list
    private static final int MIN_FILE_DESCRIPTORS = 3;

    private String delimiter;
    private OpenFilesCounter openFilesCounter;

    SortedFilesMerger(String delimiter, OpenFilesCounter openFilesCounter) {
        this.delimiter = delimiter;
        this.openFilesCounter = openFilesCounter;
    }

    File merge(List<File> files, String resultPath) throws NotEnoughFilesToOpenException {
        logger.debug("merge to {}", resultPath);
        int openFilesRequired = files.size() + 1;
        int openFilesAvailable = openFilesCounter.getAvailableCount();

        if (openFilesAvailable < openFilesRequired && openFilesAvailable < MIN_FILE_DESCRIPTORS) {
            throw new NotEnoughFilesToOpenException(openFilesAvailable, MIN_FILE_DESCRIPTORS);
        }

        int acquireCount = openFilesAvailable;

        int segmentIndex = 0;
        int filesProcessed = 0;
        File mergedSegment = null;
        while (filesProcessed < files.size()) {
            List<File> filesToMerge = new ArrayList<>();
            int segmentSize = acquireCount - 1; // -1 for result file
            if (mergedSegment != null) {
                filesToMerge.add(mergedSegment);
                segmentSize -= 1;   // -1 for mergedSegment
            }
            List<File> segment = files.subList(filesProcessed, Math.min(filesProcessed + segmentSize, files.size()));
            filesToMerge.addAll(segment);

            File prevMergedSegment = mergedSegment;
            String segmentPath = filesProcessed + segment.size() < files.size()
                    ? resultPath + "_segment_" + ++segmentIndex
                    : resultPath;   // last iteration
            mergedSegment = doMerging(filesToMerge, segmentPath);
            if (prevMergedSegment != null)
                prevMergedSegment.delete();

            filesProcessed += segment.size();
        }

        logger.debug("finished merge to {}", resultPath);

        return mergedSegment;
    }

    private File doMerging(List<File> files, String resultPath) {
        File result = Utils.createFile(resultPath);
        Map<Scanner, Integer> fileToNumber = new HashMap<>();

        try {
            FileWriter resultWriter = new FileWriter(result);
            for (File file : files) {
                Scanner scanner = new Scanner(file);
                scanner.useDelimiter(delimiter);
                fileToNumber.put(scanner, scanner.nextInt());
            }

            while (!fileToNumber.isEmpty()) {
                Map.Entry<Scanner, Integer> minEntry = null;
                for (Map.Entry<Scanner, Integer> entry : fileToNumber.entrySet()) {
                    if (minEntry == null || entry.getValue() < minEntry.getValue()) {
                        minEntry = entry;
                    }
                }

                resultWriter.write(String.valueOf(minEntry.getValue()) + delimiter);

                Scanner minScanner = minEntry.getKey();
                if (minScanner.hasNext()) {
                    fileToNumber.put(minScanner, minScanner.nextInt());
                } else {
                    minScanner.close();
                    fileToNumber.remove(minScanner);
                }
            }

            resultWriter.close();
        } catch (IOException e) {
            logger.error("merge: {}", e.getMessage(), e);
        }

        return result;
    }
}
