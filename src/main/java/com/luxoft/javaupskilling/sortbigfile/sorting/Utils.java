package com.luxoft.javaupskilling.sortbigfile.sorting;

import java.io.File;
import java.util.List;

public class Utils {

    public static File createFile(String path) {
        File result = new File(path);
        createParentDirectory(result);
        return result;
    }

    public static void createParentDirectory(File file) {
        File directory = file.getParentFile();
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    public static boolean removeFiles(List<File> files) {
        boolean result = true;
        for (File file : files) {
            result &= file.delete();
        }
        return result;
    }

}
