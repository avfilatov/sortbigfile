package com.luxoft.javaupskilling.sortbigfile.sorting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class InHeapFileSorter implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(InHeapFileSorter.class);

    private File source;
    private File target;
    private String delimiter;
    private OpenFilesCounter openFilesCounter;

    InHeapFileSorter(File source, File target, String delimiter, OpenFilesCounter openFilesCounter) {
        this.source = source;
        this.target = target;
        this.delimiter = delimiter;
        this.openFilesCounter = openFilesCounter;
    }

    @Override
    public void run() {
        sortInHeap(source, target);
    }

    private void sortInHeap(File source, File target) {
        logger.debug("sortInHeap: {}", source.getPath());
        List<Integer> numbers = readNumbersFromFile(source);
        numbers.sort(Integer::compare);
        writeNumbersToFile(target, numbers);
        logger.debug("sortInHeap finished: {}", source.getPath());
    }

    // TODO: refactoring - isolate number type related things(type, comparator)
    private List<Integer> readNumbersFromFile(File file) {
        List<Integer> result = new ArrayList<>();
        Scanner scanner = null;
        try {
            openFilesCounter.acquire(1);
            scanner = new Scanner(file);
            scanner.useDelimiter(delimiter);
            while (scanner.hasNext()){
                result.add(scanner.nextInt());
            }
        } catch (Exception e) {
            logger.error("readNumbersFromFile", e);
        } finally {
            if (scanner != null)
                scanner.close();

            openFilesCounter.release(1);
        }
        return result;
    }

    // TODO: refactoring - isolate number type related things(type, comparator)
    private void writeNumbersToFile(File file, List<Integer> numbers) {
        FileWriter writer = null;
        try {
            openFilesCounter.acquire(1);
            writer = new FileWriter(file);
            for (int number : numbers) {
                writer.write(String.valueOf(number) + delimiter);
            }
        } catch (Exception e) {
            logger.error("writeNumbersToFile: {}", e.getMessage(), e);
        } finally {
            if (writer != null)
                try {
                    writer.close();
                } catch (IOException e) {
                    logger.error("writeNumbersToFile: closing writer", e);
                }

            openFilesCounter.release(1);
        }
    }
}
