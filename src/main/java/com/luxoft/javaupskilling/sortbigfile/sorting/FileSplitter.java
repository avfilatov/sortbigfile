package com.luxoft.javaupskilling.sortbigfile.sorting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class FileSplitter {
    private static final Logger logger = LoggerFactory.getLogger(FileSplitter.class);

    private String delimiter;
    private long maxSizeBytes;


    FileSplitter(String delimiter, long maxSizeBytes) {
        this.delimiter = delimiter;
        this.maxSizeBytes = maxSizeBytes;
    }

    List<File> split(File file, String targetPath) {
        List<File> result = new ArrayList<>();

        int partIndex = 0;
        long bufferSize = 0;
        File part = null;
        FileWriter writer = null;

        try {
            Scanner scanner = new Scanner(file);
            scanner.useDelimiter(delimiter);
            while (scanner.hasNext()) {
                String nextNumber = String.valueOf(scanner.nextInt()) + delimiter;
                if ((bufferSize + nextNumber.length() > maxSizeBytes) || part == null) {
                    if (writer != null) {
                        writer.close();
                        result.add(part);
                    }
                    part = Utils.createFile(targetPath + "_part_" + ++partIndex);
                    writer = new FileWriter(part);
                    bufferSize = 0;
                }

                writer.write(nextNumber);
                bufferSize += nextNumber.length();
            }

            scanner.close();
            if (writer != null) {
                writer.close();
                result.add(part);
            }

            logger.debug("file {} splitted into {} parts", file.getPath(), partIndex);
        } catch (IOException e) {
            logger.error("splitBySize: {}", e.getMessage(), e);
        }

        return result;
    }

}
