package com.luxoft.javaupskilling.sortbigfile.sorting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SortedFileTester {
    private static final Logger logger = LoggerFactory.getLogger(SortedFileTester.class);

    private String delimiter;


    public SortedFileTester(String delimiter) {
        this.delimiter = delimiter;
    }

    boolean isSorted(File file) {
        try {
            Scanner scanner = new Scanner(file);
            scanner.useDelimiter(delimiter);

            int last = Integer.MIN_VALUE;
            while (scanner.hasNext()){
                int current = scanner.nextInt();
                if (current < last) {
                    return false;
                }
                last = current;
            }

            scanner.close();
        } catch (FileNotFoundException e) {
            logger.error("test", e);
            return false;
        }

        return true;
    }

}
