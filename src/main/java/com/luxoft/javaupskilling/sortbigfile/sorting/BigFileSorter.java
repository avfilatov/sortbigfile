package com.luxoft.javaupskilling.sortbigfile.sorting;

import com.luxoft.javaupskilling.sortbigfile.domain.FileInfo;
import com.luxoft.javaupskilling.sortbigfile.domain.FileProcessingStatus;
import com.luxoft.javaupskilling.sortbigfile.dto.FilePathDTO;
import com.luxoft.javaupskilling.sortbigfile.repository.FileRepository;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class BigFileSorter implements BigFileProcessor {
    private static final Logger logger = LoggerFactory.getLogger(BigFileSorter.class);

    private final String delimiter;
    private final long maxPartSizeBytes;
    private final int sortingThreadsCount;
    private final FileRepository fileRepository;

    private OpenFilesCounter openFilesCounter;
    private SortedFilesMerger merger;
    private FileSplitter splitter;
    private final ExecutorService executorService;

    private SortedFileTester tester;


    BigFileSorter(String delimiter, long maxPartSizeBytes, int maxOpenFiles, int sortingThreadsCount, FileRepository fileRepository) {
        this.delimiter = delimiter;
        this.maxPartSizeBytes = maxPartSizeBytes;
        this.sortingThreadsCount = sortingThreadsCount;
        this.fileRepository = fileRepository;

        openFilesCounter = new OpenFilesCounter(maxOpenFiles);
        merger = new SortedFilesMerger(delimiter, openFilesCounter);
        splitter = new FileSplitter(delimiter, maxPartSizeBytes);
        tester = new SortedFileTester(delimiter);

        executorService = Executors.newFixedThreadPool(sortingThreadsCount);
    }

    @Override
    public void shutDown() {
        executorService.shutdownNow();
    }

    @Override
    public void process(FilePathDTO filePathDTO) {
//        File inputFile = new File(filePathDTO.getPath());
//        logger.debug("Test sorting before: {} - {}}", inputFile.getPath(), tester.isSorted(inputFile) ? "passed" : "failed");

        File sortedFile = null;
        try {
            sortedFile = sort(filePathDTO);
        } catch (InterruptedException e) {
            logger.debug("process interrupted");
            return;
        }

        if (sortedFile != null) {
            if (tester.isSorted(sortedFile)) {
                logger.debug("Test sorting after: {} - PASSED", sortedFile.getPath());
            } else {
                logger.error("Test sorting after: {} - FAILED", sortedFile.getPath());
            }
        }
    }

    private File sort(FilePathDTO filePathDTO) throws InterruptedException {
        logger.debug("sort: start with {}", filePathDTO);
        File file = new File(filePathDTO.getPath());
        FileInfo fileInfo = fileRepository.getById(filePathDTO.getFileRecordId());

        startProcessing(fileInfo);

        if (!file.exists() || !file.isFile()) {
            // wrong path
            finishProcessing(fileInfo, null, FileProcessingStatus.NOT_FOUND);
            logger.error("Wrong path error: exists={}, isFile={}", file.exists(), file.isFile());
            return null;
        }

        String targetFilePath = generateResultPath(file);
        List<Callable<Object>> calls = new ArrayList<>();

        if (file.length() <= maxPartSizeBytes) {
            File targetFile = Utils.createFile(targetFilePath);
            calls.add(Executors.callable(new InHeapFileSorter(file, targetFile, delimiter, openFilesCounter)));
            executorService.invokeAll(calls);

            finishProcessing(fileInfo, targetFile, FileProcessingStatus.SUCCESS);
            logger.debug("sort: successful, {}", file);
            return targetFile;
        }

        List<File> parts = splitter.split(file, targetFilePath);
        for (File part : parts) {
            calls.add(Executors.callable(new InHeapFileSorter(part, part, delimiter, openFilesCounter)));
        }
        executorService.invokeAll(calls);

//        for (File part : parts) {
//            logger.debug("Test sorting part: {} - {}}", part.getPath(), tester.isSorted(part) ? "passed" : "failed");
//        }

        File sortedFile = null;
        try {
            sortedFile = merger.merge(parts, targetFilePath);
        } catch (NotEnoughFilesToOpenException e) {
            finishProcessing(fileInfo, null, FileProcessingStatus.FAILURE);
            logger.error("sort: failure, {}", file, e);
            return null;
        } finally {
            Utils.removeFiles(parts);
        }

        finishProcessing(fileInfo, sortedFile, FileProcessingStatus.SUCCESS);
        logger.debug("sort: successful, {}", file);
        return sortedFile;
    }

    private String generateResultPath(File sourceFile) {
        // TODO: implement
        String baseName = FilenameUtils.getBaseName(sourceFile.getName());
        String extension = FilenameUtils.getExtension(sourceFile.getName());
//        String homeDir = System.getenv("CATALINA_HOME");
//        String path = homeDir + "/webapps/sortbigfile-output/" + baseName + "_" + sourceFile.lastModified() + "." + extension;
//        return path;
        return "D:" + File.separator + "sortbigfile-data" + File.separator + "output" + File.separator
                + baseName + "_" + sourceFile.lastModified() + "." + extension;
    }

    private void startProcessing(FileInfo fileInfo) {
        fileInfo.startProcessing(System.currentTimeMillis());
        fileRepository.update(fileInfo);
    }

    private void finishProcessing(FileInfo fileInfo, File sortedFile, String status) {
        fileInfo.finishProcessing(System.currentTimeMillis());
        String targetPath = sortedFile != null ? sortedFile.getPath() : null;
        String targetUrl = sortedFile != null ? "/files/get/" + fileInfo.getId() : null;
        fileInfo.update(status, targetPath, targetUrl);
        fileRepository.update(fileInfo);
    }
}
