package com.luxoft.javaupskilling.sortbigfile.sorting;

import com.luxoft.javaupskilling.sortbigfile.dto.FilePathDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

class BigFileGenerator implements BigFileProcessor {

    private static final Logger logger = LoggerFactory.getLogger(BigFileGenerator.class);

    private String delimiter;
    private final long targetFileSizeBytes;


    BigFileGenerator(String delimiter, long targetFileSizeBytes) {
        this.delimiter = delimiter;
        this.targetFileSizeBytes = targetFileSizeBytes;
    }

    @Override
    public void process(FilePathDTO filePathDTO) {
        logger.debug("generateSourceFile: start with {}", filePathDTO);
        File file = Utils.createFile(filePathDTO.getPath());
        FileWriter writer = null;

        Random random = new Random();

        try {
            writer = new FileWriter(file);
            long size = 0;
            while (size < targetFileSizeBytes) {
                String number = String.valueOf(random.nextInt()) + delimiter;
                writer.write(number);
                size += number.length();
            }
        } catch (IOException e) {
            logger.error("generateSourceFile", e);
        } finally {
            if (writer != null)
                try {
                    writer.close();
                } catch (IOException e) {
                    logger.error("generateSourceFile: closing writer", e);
                }

        }

        logger.debug("generateSourceFile: successful, {}", file);
    }

    @Override
    public void shutDown() {

    }
}
