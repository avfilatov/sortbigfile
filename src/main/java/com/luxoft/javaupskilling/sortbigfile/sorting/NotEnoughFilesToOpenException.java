package com.luxoft.javaupskilling.sortbigfile.sorting;

class NotEnoughFilesToOpenException extends Exception {

    NotEnoughFilesToOpenException(int availableFiles, int minRequiredFiles) {
        super(String.format("Not enough file descriptors: %d. Need at least %d.", availableFiles, minRequiredFiles));
    }

}
