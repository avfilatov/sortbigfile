package com.luxoft.javaupskilling.sortbigfile.sorting;

import com.luxoft.javaupskilling.sortbigfile.dto.FilePathDTO;

interface BigFileProcessor {

    void process(FilePathDTO filePathDTO);

    void shutDown();

}
