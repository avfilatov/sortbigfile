package com.luxoft.javaupskilling.sortbigfile.sorting;

class OpenFilesCounter {

    private final int maxOpenFiles;
    private volatile int openFiles;

    OpenFilesCounter(int maxOpenFiles) {
        this.maxOpenFiles = maxOpenFiles;
        openFiles = 0;
    }

    int getAvailableCount() {
        return maxOpenFiles - openFiles;
    }

    synchronized void acquire(int count) throws InterruptedException {
        if (getAvailableCount() < count) {
            wait();
        }

        openFiles += count;
    }

    synchronized void release(int count) {
        openFiles = Math.max(openFiles - count, 0);
        notifyAll();
    }

}
