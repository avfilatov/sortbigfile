package com.luxoft.javaupskilling.sortbigfile.sorting;

import com.luxoft.javaupskilling.sortbigfile.domain.FileActionType;
import com.luxoft.javaupskilling.sortbigfile.domain.FileInfo;
import com.luxoft.javaupskilling.sortbigfile.dto.FilePathDTO;
import com.luxoft.javaupskilling.sortbigfile.repository.FileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

@Component
public class BigFileService implements InitializingBean, DisposableBean {

    private static final Logger logger = LoggerFactory.getLogger(BigFileService.class);

    private static final int DEFAULT_MAX_OPEN_FILES = 100;
    private static final int DEFAULT_SORTING_THREADS_COUNT = 1;
    private static final String DELIMITER = " ";

    @Autowired
    private FileRepository fileRepository;

    private final BlockingDeque<FilePathDTO> queue;
    private QueueConsumer queueConsumer = null;

    private final int maxOpenFiles;
    private final int sortingThreadsCount;
    private final long maxPartSizeBytes;
    private final long generatedFileSizeBytes;


    public BigFileService() {
        String maxOpenFilesStr = System.getProperty("maxOpenFiles");
        maxOpenFiles = maxOpenFilesStr != null ? Integer.parseInt(maxOpenFilesStr) : DEFAULT_MAX_OPEN_FILES;

        String sortingThreads = System.getProperty("sortingThreads");
        sortingThreadsCount = sortingThreads != null ? Integer.parseInt(sortingThreads) : DEFAULT_SORTING_THREADS_COUNT;

        long maxHeapSize = Runtime.getRuntime().maxMemory();
        long freeMemory = Runtime.getRuntime().freeMemory();
        maxPartSizeBytes = freeMemory / 4 / sortingThreadsCount;
        generatedFileSizeBytes = 2 * maxHeapSize;
        logger.debug("sortingThreads={}, maxHeapSize = {} Mb, freeMemory = {} Mb", sortingThreadsCount, maxHeapSize / 1024 / 1024, freeMemory / 1024 / 1024);

        queue = new LinkedBlockingDeque<>();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        queueConsumer = new QueueConsumer(queue);
        queueConsumer.start();
    }

    @Override
    public void destroy() throws Exception {
        if (queueConsumer != null) {
            queueConsumer.shutDown();
        }
    }

    public void process(FilePathDTO filePathDTO) {
        FileInfo fileInfo = insertRecord(filePathDTO, filePathDTO.getUserId());
        filePathDTO.setFileRecordId(fileInfo.getId());
        queue.offer(filePathDTO);
        logger.debug("process: file is waiting for processing {}", filePathDTO);
    }

    private FileInfo insertRecord(FilePathDTO filePathDTO, int userId) {
        File file = new File(filePathDTO.getPath());
        FileInfo fileInfo = new FileInfo(userId, file.getPath(), file.length(), file.lastModified());
        fileInfo.setEnterQueueTime(System.currentTimeMillis());
        fileRepository.insert(fileInfo);
        return fileInfo;
    }


    private class QueueConsumer extends Thread {

        private final BlockingQueue<FilePathDTO> queue;
        private volatile boolean isRunning = false;

        private final Map<String, BigFileProcessor> fileProcessors;


        public QueueConsumer(BlockingQueue<FilePathDTO> queue) {
            this.queue = queue;

            fileProcessors = new HashMap<>();
            fileProcessors.put(FileActionType.GENERATE, new BigFileGenerator(DELIMITER, generatedFileSizeBytes));
            fileProcessors.put(FileActionType.SORT, new BigFileSorter(DELIMITER, maxPartSizeBytes, maxOpenFiles, sortingThreadsCount, fileRepository));
        }

        public void shutDown() {
            isRunning = false;

            for (Map.Entry<String, BigFileProcessor> processorEntry : fileProcessors.entrySet()) {
                processorEntry.getValue().shutDown();
            }

            interrupt();
        }

        @Override
        public void run() {
            isRunning = true;
            try {
                while (isRunning) {
                    FilePathDTO filePathDTO = queue.take();
                    BigFileProcessor processor = fileProcessors.get(filePathDTO.getAction());
                    processor.process(filePathDTO);
                }
            } catch (InterruptedException e) {
                logger.debug("run: interrupted");
            } catch (Exception e) {
                logger.error("run: {}", e.getMessage(), e);
            }
        }

    }

}
