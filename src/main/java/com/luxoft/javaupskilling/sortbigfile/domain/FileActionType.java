package com.luxoft.javaupskilling.sortbigfile.domain;

public interface FileActionType {

    String SORT = "sort";
    String GENERATE = "generate";

}
