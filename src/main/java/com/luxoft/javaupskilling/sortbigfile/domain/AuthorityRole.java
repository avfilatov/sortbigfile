package com.luxoft.javaupskilling.sortbigfile.domain;

public class AuthorityRole {

    public static final String USER = "ROLE_USER";
    public static final String ADMIN = "ROLE_ADMIN";

    public static String getUSER() {
        return USER;
    }

    public static String getADMIN() {
        return ADMIN;
    }
}
