package com.luxoft.javaupskilling.sortbigfile.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="app_user")
public class AppUser {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="login")
    private String login;

    @Column(name="password")
    private String password;

    @Column(name="name")
    private String name;

    @Column(name="enabled")
    private boolean enabled;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "app_user_role",
            joinColumns = {
                    @JoinColumn(name = "id", referencedColumnName = "id")
            }
    )
    private Set<AppUserRole> roles;


    public AppUser() {
        enabled = true;
    }


    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<AppUserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<AppUserRole> roles) {
        this.roles = roles;
    }

    public boolean hasRole(String authorityRole) {
        return roles.stream().anyMatch(appUserRole -> appUserRole.getAuthorityRole().equals(authorityRole));
    }

    @Override
    public String toString() {
        return "AppUser{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", enabled=" + enabled +
                ", roles=" + roles +
                '}';
    }
}
