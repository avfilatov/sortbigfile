package com.luxoft.javaupskilling.sortbigfile.domain;

import javax.persistence.*;

@Entity
@Table(name="files")
public class FileInfo {

    private static final long MAX_BYTES_TO_HAVE_URL = 1024 * 1024 * 10;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="sender")
    private int sender;

    @Column(name="source_path")
    private String sourcePath;

    @Column(name="size")
    private long size;

    @Column(name="last_modified")
    private long lastModified;

    @Column(name="status")
    private String status;

    @Column(name="target_path")
    private String targetPath;

    @Column(name="target_url")
    private String targetUrl;

    @Column(name="enter_queue")
    private long enterQueueTime;

    @Column(name="start_processing")
    private long startProcessingTime;

    @Column(name="finish_processing")
    private long finishProcessingTime;

    public FileInfo() {
    }

    public FileInfo(int sender, String sourcePath, long size, long lastModified) {
        this.sender = sender;
        this.sourcePath = sourcePath;
        this.size = size;
        this.lastModified = lastModified;

        this.status = FileProcessingStatus.WAITING;
        this.startProcessingTime = 0;
        this.finishProcessingTime = 0;
        this.enterQueueTime = 0;
        this.targetPath = null;
        this.targetUrl = null;
    }

    public boolean canHaveUrl() {
        return size <= MAX_BYTES_TO_HAVE_URL;
    }

    public void update(String status, String targetPath, String targetUrl) {
        this.status = status;
        this.targetPath = targetPath;
        this.targetUrl = targetUrl;
    }

    public void startProcessing(long time) {
        status = FileProcessingStatus.IN_PROCESS;
        startProcessingTime = time;
    }

    public void finishProcessing(long time) {
        status = FileProcessingStatus.SUCCESS;
        finishProcessingTime = time;
    }

    public int getId() {
        return id;
    }

    public int getSender() {
        return sender;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public long getSize() {
        return size;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }

    public long getStartProcessingTime() {
        return startProcessingTime;
    }

    public long getFinishProcessingTime() {
        return finishProcessingTime;
    }

    public long getEnterQueueTime() {
        return enterQueueTime;
    }

    public void setEnterQueueTime(long enterQueueTime) {
        this.enterQueueTime = enterQueueTime;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "id=" + id +
                ", sender=" + sender +
                ", sourcePath='" + sourcePath + '\'' +
                ", size=" + size +
                ", lastModified=" + lastModified +
                ", status='" + status + '\'' +
                ", targetPath='" + targetPath + '\'' +
                ", targetUrl='" + targetUrl + '\'' +
                '}';
    }
}
