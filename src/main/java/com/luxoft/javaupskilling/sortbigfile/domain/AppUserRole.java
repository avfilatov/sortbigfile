package com.luxoft.javaupskilling.sortbigfile.domain;

import javax.persistence.*;

@Embeddable
public class AppUserRole {
    @Column(name = "role")
    private String authorityRole;

    public AppUserRole() {
    }

    public AppUserRole(String authorityRole) {
        this.authorityRole = authorityRole;
    }

    public String getAuthorityRole() {
        return authorityRole;
    }

    public void setAuthorityRole(String authorityRole) {
        this.authorityRole = authorityRole;
    }

    @Override
    public String toString() {
        return "AppUserRole{" +
                "authorityRole='" + authorityRole + '\'' +
                '}';
    }
}
