package com.luxoft.javaupskilling.sortbigfile.domain;

public interface FileProcessingStatus {

    public static final String WAITING = "waiting";
    public static final String IN_PROCESS = "in_process";
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String NOT_FOUND = "not_found";
    public static final String INTERRUPTED = "interrupted";

}
