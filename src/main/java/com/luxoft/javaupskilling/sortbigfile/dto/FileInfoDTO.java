package com.luxoft.javaupskilling.sortbigfile.dto;

public class FileInfoDTO {

    private final int id;
    private final int sender;
    private final String sourcePath;
    private final String size;
    private final String lastModified;
    private final String status;
    private final String enterQueue;
    private final String startProcessing;
    private final String finishProcessing;
    private final String targetPath;
    private final String targetUrl;

    public FileInfoDTO(int id,
                       int sender,
                       String sourcePath,
                       String size,
                       String lastModified,
                       String status,
                       String enterQueue,
                       String startProcessing,
                       String finishProcessing,
                       String targetPath,
                       String targetUrl) {
        this.id = id;
        this.sender = sender;
        this.sourcePath = sourcePath;
        this.size = size;
        this.lastModified = lastModified;
        this.status = status;
        this.enterQueue = enterQueue;
        this.startProcessing = startProcessing;
        this.finishProcessing = finishProcessing;
        this.targetPath = targetPath;
        this.targetUrl = targetUrl;
    }

    public int getId() {
        return id;
    }

    public int getSender() {
        return sender;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public String getSize() {
        return size;
    }

    public String getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }

    public String getEnterQueue() {
        return enterQueue;
    }

    public String getStartProcessing() {
        return startProcessing;
    }

    public String getFinishProcessing() {
        return finishProcessing;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public String getTargetUrl() {
        return targetUrl;
    }
}
