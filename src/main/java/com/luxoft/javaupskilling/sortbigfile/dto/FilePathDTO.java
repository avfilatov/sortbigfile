package com.luxoft.javaupskilling.sortbigfile.dto;

import java.util.UnknownFormatConversionException;

public class FilePathDTO {

    public static FilePathDTO fromString(String input) {
        String[] pairs = input.split("&");
        if (pairs.length == 0)
            throw new UnknownFormatConversionException(input);

        String path = null;
        String action = null;
        int userId = 0;

        for (String pair : pairs) {
            String[] kv = pair.split("=");
            if (kv.length != 2)
                throw new UnknownFormatConversionException(pair);

            if (kv[0].equals("path"))
                path = kv[1];

            if (kv[0].equals("action"))
                action = kv[1];

            if (kv[0].equals("user"))
                userId = Integer.parseInt(kv[1]);
        }

        return new FilePathDTO(path, action, userId);
    }


    private int fileRecordId;
    private String path;
    private String action;
    private int userId;

    public FilePathDTO(String path, String action, int user) {
        if (path == null || action == null || user == 0) {
            throw new IllegalArgumentException("FilePathDTO: path and user can't be null");
        }
        this.path = path;
        this.action = action;
        this.userId = user;
    }

    public String toString() {
        return String.format("path=%s&user=%d&action=%s", path, userId, action);
    }

    public String getPath() {
        return path;
    }

    public int getUserId() {
        return userId;
    }

    public String getAction() {
        return action;
    }

    public int getFileRecordId() {
        return fileRecordId;
    }

    public void setFileRecordId(int fileRecordId) {
        this.fileRecordId = fileRecordId;
    }
}
