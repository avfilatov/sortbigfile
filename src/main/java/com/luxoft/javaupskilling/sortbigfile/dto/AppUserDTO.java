package com.luxoft.javaupskilling.sortbigfile.dto;

import java.util.List;

public class AppUserDTO {

    private final int id;
    private final String login;
    private final String name;
    private final boolean enabled;
    private final List<String> roles;

    public AppUserDTO(int id, String login, String name, boolean enabled, List<String> roles) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.enabled = enabled;
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public List<String> getRoles() {
        return roles;
    }
}
