package com.luxoft.javaupskilling.sortbigfile.repository;

import com.luxoft.javaupskilling.sortbigfile.domain.AppUser;
import com.luxoft.javaupskilling.sortbigfile.domain.FileInfo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class FileRepository {

    @PersistenceContext
    private EntityManager em;

    public FileInfo getById(int id) {
        return em.find(FileInfo.class, id);
    }

    public List<FileInfo> getAll() {
        return em.createQuery("SELECT f FROM FileInfo f ORDER BY f.enterQueueTime DESC", FileInfo.class).getResultList();
    }

    public List<FileInfo> getByUser(AppUser appuser) {
        return em.createQuery(String.format("SELECT f FROM FileInfo f WHERE f.sender=%d ORDER BY f.enterQueueTime DESC", appuser.getId()), FileInfo.class).getResultList();
    }

    public void insert(FileInfo fileInfo) {
        em.persist(fileInfo);
    }

    public void update(FileInfo fileInfo) {
        em.merge(fileInfo);
    }

    public void remove(FileInfo fileInfo) {
        em.remove(fileInfo);
    }

    public void remove(List<FileInfo> fileInfoList) {
        for (FileInfo fileInfo : fileInfoList)
            em.remove(fileInfo);
    }

}
