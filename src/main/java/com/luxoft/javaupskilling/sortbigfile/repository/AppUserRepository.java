package com.luxoft.javaupskilling.sortbigfile.repository;

import com.luxoft.javaupskilling.sortbigfile.domain.AppUser;
import com.luxoft.javaupskilling.sortbigfile.domain.FileInfo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class AppUserRepository {

    @PersistenceContext
    private EntityManager em;

    public AppUser getById(int id) {
        return em.find(AppUser.class, id);
    }

    public AppUser getByLogin(String login) {
        return em.createQuery(String.format("SELECT au FROM AppUser au WHERE au.login='%s'", login), AppUser.class).getSingleResult();
    }

    public List<AppUser> getAll() {
        return em.createQuery("SELECT au FROM AppUser au", AppUser.class).getResultList();
    }

    public void insert(AppUser appUser) {
        em.persist(appUser);
    }

    public void remove(AppUser appUser) {
        em.remove(appUser);
    }

    public void remove(String login) {
        AppUser appUser = getByLogin(login);
        em.remove(appUser);
    }

    public void update(AppUser appUser) {
        em.merge(appUser);
    }

}
