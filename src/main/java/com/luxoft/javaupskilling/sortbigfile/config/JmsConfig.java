package com.luxoft.javaupskilling.sortbigfile.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.QueueConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

@Configuration
@EnableJms
@ComponentScan("com.luxoft.javaupskilling.sortbigfile")
@PropertySource("classpath:/application.properties")
public class JmsConfig {

    @Value("${jms.initial_context_factory}")
    private String jmsInitialContextFactory;

    @Value("${jms.provider_url}")
    private String jmsProviderUrl;

    @Bean
    public JmsListenerContainerFactory<?> jmsListenerTopicFactory() {
        DefaultJmsListenerContainerFactory factory
                = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setPubSubDomain(true);
//        factory.setClientId("MyDurableClientId");
//        factory.setSubscriptionDurable(true);
        return factory;
    }

    @Bean
    public JmsListenerContainerFactory<?> jmsListenerQueueFactory() {
        DefaultJmsListenerContainerFactory factory
                = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        return factory;
    }

    @Bean
    public ConnectionFactory connectionFactory () {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, jmsInitialContextFactory);
        props.put (Context.PROVIDER_URL, jmsProviderUrl);

        ConnectionFactory connectionFactory = null;
        try {
            Context ctx = new InitialContext(props);
            connectionFactory = (QueueConnectionFactory) ctx.lookup("ConnectionFactory");
        } catch (NamingException e) {
            System.out.println(e);
        }

        return connectionFactory;
    }

    @Bean
    public JmsTemplate jmsTemplate() {
        JmsTemplate result = new JmsTemplate(connectionFactory());
        return result;
    }

}
