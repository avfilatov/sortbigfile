package com.luxoft.javaupskilling.sortbigfile.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

public class Initializer implements WebApplicationInitializer {

    private static final String DISPATCHER_SERVLET_NAME = "dispatcher";


    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException {

        AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();

        appContext.scan("com.luxoft.javaupskilling.sortbigfile");
        servletContext.addListener(new ContextLoaderListener(appContext));

        appContext.setServletContext(servletContext);

        Dynamic appServlet = servletContext.addServlet(DISPATCHER_SERVLET_NAME, new DispatcherServlet(appContext));
        appServlet.addMapping("/");
        appServlet.setLoadOnStartup(1);
    }
}
