package com.luxoft.javaupskilling.sortbigfile.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource("classpath:/application.properties")
public class AppConfig {

    @Value("${hibernate.dialect}")
    public String hibernateDialect;

    @Value("${hibernate.show_sql}")
    public String jpaShowSql;

    @Value("${hikaricp.properties}")
    public String hikaricpProperties;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
