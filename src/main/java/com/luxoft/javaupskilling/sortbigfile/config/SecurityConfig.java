package com.luxoft.javaupskilling.sortbigfile.config;

import com.luxoft.javaupskilling.sortbigfile.domain.AuthorityRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
@ComponentScan("com.luxoft.javaupskilling.sortbigfile")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("userDetailsService")
    UserDetailsService userDetailsService;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // HTTP Security
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/users/**").access(String.format("hasRole('%s')", AuthorityRole.ADMIN))
                .antMatchers("/lbClassicStatus").access(String.format("hasRole('%s')", AuthorityRole.ADMIN))
                .antMatchers("/**").access(String.format("hasRole('%s')", AuthorityRole.USER))
                .and()
                    .formLogin()
                    .loginProcessingUrl("/login")
                    .defaultSuccessUrl("/")
                    .failureUrl("/login?error")
                    .usernameParameter("username").passwordParameter("password")
//                .and()
//                    .logout()
//                    // разрешаем делать логаут всем
//                    .permitAll()
//                    // указываем URL логаута
//                    .logoutUrl("/logout")
//                    // указываем URL при удачном логауте
//                    .logoutSuccessUrl("/login?logout")
//                    // делаем не валидной текущую сессию
//                    .invalidateHttpSession(true);
                ;
    }

}