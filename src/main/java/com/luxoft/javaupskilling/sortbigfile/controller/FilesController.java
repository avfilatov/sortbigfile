package com.luxoft.javaupskilling.sortbigfile.controller;

import com.luxoft.javaupskilling.sortbigfile.domain.AppUser;
import com.luxoft.javaupskilling.sortbigfile.domain.AuthorityRole;
import com.luxoft.javaupskilling.sortbigfile.domain.FileActionType;
import com.luxoft.javaupskilling.sortbigfile.domain.FileInfo;
import com.luxoft.javaupskilling.sortbigfile.dto.FileInfoDTO;
import com.luxoft.javaupskilling.sortbigfile.dto.FilePathDTO;
import com.luxoft.javaupskilling.sortbigfile.repository.AppUserRepository;
import com.luxoft.javaupskilling.sortbigfile.repository.FileRepository;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.helpers.AbstractUnmarshallerImpl;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/files")
public class FilesController {

    private static final Logger logger = LoggerFactory.getLogger(FilesController.class);

    private static String millisecondsToTimeString(long milliseconds) {
        return milliseconds > 0 ? FileTime.from(milliseconds, TimeUnit.MILLISECONDS).toString() : "";
    }


    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private FileRepository fileRepository;

    @GetMapping
    public String start(Model model) {
        AppUser appUser = getCurrentAppUser();
        List<FileInfo> fileInfoList = null;
        if (appUser.hasRole(AuthorityRole.ADMIN)) {
            fileInfoList = fileRepository.getAll();
        } else if (appUser.hasRole(AuthorityRole.USER)) {
            fileInfoList = fileRepository.getByUser(appUser);
        }

        List<FileInfoDTO> fileInfoDTOList = new ArrayList<>();
        for (FileInfo fileInfo : fileInfoList) {
            fileInfoDTOList.add(new FileInfoDTO(
                    fileInfo.getId(),
                    fileInfo.getSender(),
                    fileInfo.getSourcePath(),
                    FileUtils.byteCountToDisplaySize(fileInfo.getSize()),
                    millisecondsToTimeString(fileInfo.getLastModified()),
                    fileInfo.getStatus(),
                    millisecondsToTimeString(fileInfo.getEnterQueueTime()),
                    millisecondsToTimeString(fileInfo.getStartProcessingTime()),
                    millisecondsToTimeString(fileInfo.getFinishProcessingTime()),
                    fileInfo.getTargetPath(),
                    fileInfo.getTargetUrl()
            ));
        }

        model.addAttribute("fileInfoList", fileInfoDTOList);
        return "files";
    }

    @PostMapping("sort")
    public RedirectView sendFileToSorting(@RequestParam(name = "pathToFile") String pathToFile,
                                   @RequestParam(name = "actionType") String actionType,
                                   RedirectAttributes redirectAttributes,
                                   Model model) {

        RedirectView rv = new RedirectView("/files", true);

        File file = new File(pathToFile);
        if (actionType.equals(FileActionType.SORT) && (!file.exists() || !file.isFile())) {
            logger.error("sendFileToSorting: file path error, exists={}, isFile={}", file.exists(), file.isFile());
            redirectAttributes.addFlashAttribute("error", "Error: file does not exist or not a file");
            redirectAttributes.addFlashAttribute("status", "failure");
            return rv;
        }

        AppUser appUser = getCurrentAppUser();
        FilePathDTO filePathDTO = new FilePathDTO(pathToFile, actionType, appUser.getId());
        logger.info("sendFileToSorting: {}", filePathDTO.toString());
        jmsTemplate.send("test.queue", new MessageCreator() {
            public Message createMessage(Session session) throws JMSException {
                TextMessage message = session.createTextMessage(filePathDTO.toString());
                return message;
            }
        });
        redirectAttributes.addFlashAttribute("status", "success");
        return rv;
    }

    public AppUser getCurrentAppUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return appUserRepository.getByLogin(auth.getName());
    }

    @GetMapping("get/{id}")
    @ResponseBody
    public ResponseEntity<Resource> get(@PathVariable("id") int fileInfoId) {
        FileInfo fileInfo = fileRepository.getById(fileInfoId);

        if (fileInfo == null || fileInfo.getTargetPath() == null) {
            return ResponseEntity.notFound().build();
        }

        File file = new File(fileInfo.getTargetPath());

        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            logger.error("get", e);
            return ResponseEntity.notFound().build();
        }

        InputStreamResource resource = new InputStreamResource(inputStream);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=\"%s\"", file.getName()));

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .contentLength(file.length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

}
