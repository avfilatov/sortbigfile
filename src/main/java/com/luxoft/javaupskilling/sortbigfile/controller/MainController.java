package com.luxoft.javaupskilling.sortbigfile.controller;

import com.luxoft.javaupskilling.sortbigfile.domain.AppUser;
import com.luxoft.javaupskilling.sortbigfile.repository.AppUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private AppUserRepository appUserRepository;

    @GetMapping
    public String start(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        auth.getAuthorities();
        AppUser appUser = appUserRepository.getByLogin(name);

        StringBuilder userRolesView = new StringBuilder();
        for (GrantedAuthority authority : auth.getAuthorities()) {
            if (userRolesView.length() > 0)
                userRolesView.append("; ");

            userRolesView.append(authority.getAuthority());
        }

        model.addAttribute("userName", appUser.getName());
        model.addAttribute("userRoles", userRolesView.toString());

        logger.info("start: appUser={}", appUser);

        return "index";
    }

}
