package com.luxoft.javaupskilling.sortbigfile.controller;

import com.luxoft.javaupskilling.sortbigfile.domain.AppUser;
import com.luxoft.javaupskilling.sortbigfile.domain.AppUserRole;
import com.luxoft.javaupskilling.sortbigfile.domain.FileInfo;
import com.luxoft.javaupskilling.sortbigfile.dto.AppUserDTO;
import com.luxoft.javaupskilling.sortbigfile.repository.AppUserRepository;
import com.luxoft.javaupskilling.sortbigfile.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("users")
public class UsersController {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private FileRepository fileRepository;


    @GetMapping
    public String start(Model model) {
        List<AppUser> appUsers = appUserRepository.getAll();
        List<AppUserDTO> appUserDTOList = new ArrayList<>();

        for (AppUser appUser : appUsers) {
            AppUserDTO dto = new AppUserDTO(
                    appUser.getId(),
                    appUser.getLogin(),
                    appUser.getName(),
                    appUser.isEnabled(),
                    appUser.getRoles().stream().map(appUserRole -> appUserRole.getAuthorityRole()).collect(Collectors.toList())
            );
            appUserDTOList.add(dto);
        }

        model.addAttribute("appUsers", appUserDTOList);
        return "users";
    }

    @GetMapping("/add")
    public String testSecurity(Model model) {
        model.addAttribute("message", "You are on Add User page");
        return "users";
    }

    @PostMapping("/add")
    public RedirectView addUser(@RequestParam(name="login") String login,
                                @RequestParam(name="password") String password,
                                @RequestParam(name="confirm_password") String confirmPassword,
                                @RequestParam(name="name") String name,
                                @RequestParam(name="roles") String roles,
                                RedirectAttributes redirectAttributes) {

        RedirectView rv = new RedirectView("/users", true);

        if (!password.equals(confirmPassword)) {
            redirectAttributes.addFlashAttribute("error", "Error: Password is not equal to Confirm password");
            return rv;
        }

        AppUser appUser = new AppUser();
        appUser.setLogin(login);
        String encodedPswd = passwordEncoder.encode(password);
        appUser.setPassword(encodedPswd);
        appUser.setName(name);
        appUser.setRoles(
                Arrays.stream(roles.split(","))
                        .map(role -> new AppUserRole(role))
                        .collect(Collectors.toSet())
        );
        appUser.setEnabled(true);
        appUserRepository.insert(appUser);

        redirectAttributes.addFlashAttribute("message", String.format("User %s was successfully added", login));
        return rv;
    }

    @PostMapping("/remove")
    public RedirectView removeUser(@RequestParam(name="login") String login,
                                 RedirectAttributes redirectAttributes) {

        RedirectView rv = new RedirectView("/users", true);
        appUserRepository.remove(login);
        redirectAttributes.addFlashAttribute("message", String.format("User %s was successfully removed", login));
        return rv;
    }

    @PostMapping("/enable")
    public RedirectView enableUser(@RequestParam(name="login") String login,
                                 RedirectAttributes redirectAttributes) {

        RedirectView rv = new RedirectView("/users", true);
        updateEnabled(login, true);
        redirectAttributes.addFlashAttribute("message", String.format("User %s was successfully enabled", login));
        return rv;
    }

    @PostMapping("/disable")
    public RedirectView disableUser(@RequestParam(name="login") String login,
                                 RedirectAttributes redirectAttributes) {

        RedirectView rv = new RedirectView("/users", true);
        updateEnabled(login, false);
        redirectAttributes.addFlashAttribute("message", String.format("User %s was successfully disabled", login));
        return rv;
    }

    private void updateEnabled(String login, boolean enabled) {
        AppUser appUser = appUserRepository.getByLogin(login);
        appUser.setEnabled(enabled);
        appUserRepository.update(appUser);
    }


}
