package com.luxoft.javaupskilling.sortbigfile.service;

import com.luxoft.javaupskilling.sortbigfile.dto.FilePathDTO;
import com.luxoft.javaupskilling.sortbigfile.sorting.BigFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Component
public class JmsReceiver {

    private static final Logger logger = LoggerFactory.getLogger(JmsReceiver.class);

    @Autowired
    private BigFileService bigFileService;

    @JmsListener(destination = "test.queue", containerFactory = "jmsListenerQueueFactory")
    public void receiveMessageFromQueue(Message message) {
        receiveMessage(message);
    }

    @JmsListener(destination = "test.topic", containerFactory = "jmsListenerTopicFactory")
    public void receiveMessageFromTopic(Message message) {
        receiveMessage(message);
    }

    public void receiveMessage(Message message) {
        String msgText = null;

        if (message instanceof TextMessage) {
            TextMessage tm = (TextMessage) message;
            try {
                msgText = tm.getText();
            } catch (JMSException e) {
                logger.error("receiveMessage:", e);
                throw new RuntimeException(e);
            }
        } else {
            logger.error("receiveMessage: unexpected message type {}", message);
        }

        logger.info("receiveMessage: {}", message);

        if (msgText != null) {
            handlePath(FilePathDTO.fromString(msgText));
        }
    }

    private void handlePath(FilePathDTO pathToFile) {
        bigFileService.process(pathToFile);
    }

}
