package com.luxoft.javaupskilling.sortbigfile.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.luxoft.javaupskilling.sortbigfile.domain.AppUser;
import com.luxoft.javaupskilling.sortbigfile.domain.AppUserRole;
import com.luxoft.javaupskilling.sortbigfile.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
@ComponentScan("com.luxoft.javaupskilling.sortbigfile")
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Transactional(readOnly=true)
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        AppUser user = appUserRepository.getByLogin(username);
        List<GrantedAuthority> authorities = buildUserAuthority(user.getRoles());
        return buildUserForAuthentication(user, authorities);
    }

    private User buildUserForAuthentication(AppUser user,
                                            List<GrantedAuthority> authorities) {
        return new User(user.getLogin(), user.getPassword(), user.isEnabled(),
                true, true, true, authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(Set<AppUserRole> userRoles) {
        List<GrantedAuthority> result = new ArrayList<>();
        for (AppUserRole userRole : userRoles) {
            result.add(new SimpleGrantedAuthority(userRole.getAuthorityRole()));
        }
        return result;
    }

}